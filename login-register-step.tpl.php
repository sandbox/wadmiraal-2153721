<?php

/**
 * @file
 * Default theme implementation to display the login/register form.
 *
 * Available variables:
 * - $login_form-: User login form.
 * - $register_form: User register form. This variable can be NULL if the user has not the
 *   right to create an account without admin approval.
 *
 * @see template_preprocess()
 * @see template_preprocess_login_register_step()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div class="login-register-step-page login-register-step-forms-wrapper">
  <div class="login-register-step-user-form login-register-step-user-login-form-wrapper">
    <h3><?php print t("Login"); ?></h3>
    <?php print render($login_form); ?>
  </div>

  <?php if (!empty($register_form)): ?>
    <div class="login-register-step-user-form-gutter">
      <?php print t("or"); ?>
    </div>

    <div class="login-register-step-user-form login-register-step-user-register-form-wrapper">
      <h3><?php print t("Create an account"); ?></h3>
      <?php print render($register_form); ?>
    </div>
  <?php endif; ?>
</div>